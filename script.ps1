param(
  [string]$templateName,
  [string]$version
)

# import the specific module housing the AzTemplateSpec cmdlets
Import-Module -Name 'Az.Resources' -Force

# Get the Resource Group housing the Template Specs.
$resourceGroup = Get-AzResourceGroup -ResourceGroupName 'DevelopmentSupport'

# Use New-AzTemplateSpec to create/update the template spec.
New-AzTemplateSpec `
-Name $templateName `
-TemplateFile template.json `
-Version $version `
-Location $resourceGroup.Location `
-ResourceGroupName $resourceGroup.ResourceGroupName `
-Force

$message = "Deployed the {0} template as version {1}" -f $templateName,$version
Write-Host $message
